
require 'active_record'


class Restaurant < ActiveRecord::Base
  has_many :tables
end

class Table < ActiveRecord::Base
  belongs_to :restaurants
end

class User < ActiveRecord::Base
end

class Reservation < ActiveRecord::Base
  has_one :tables
  has_one :users
end
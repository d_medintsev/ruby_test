class CreateReservations < ActiveRecord::Migration[6.0]
  def change
    create_table :reservations do |t|
      t.index :id
      t.string :client
      t.time :reservation_time
    end

    add_foreign_key :reservations, :tables, column: :id
  end
end

class CreateRestaurants < ActiveRecord::Migration[6.0]
  def change
    create_table :restaurants do |t|
      t.string :name
      t.string :address
      t.string :open, :default => "10:00"
      t.string :close, :default => "23:00"
    end
  end
end

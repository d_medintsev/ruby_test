class UpdeteRestaurants < ActiveRecord::Migration[6.0]
  def change
    remove_column :restaurants, :open
    remove_column :restaurants, :close
    add_column :restaurants, :open, :time, :default => "09:00"
    add_column :restaurants, :close, :time, :default => "23:00"
  end
end


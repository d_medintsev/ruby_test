class UpdateReservations01 < ActiveRecord::Migration[6.0]
  def change
    remove_column :reservations, :reservation_time
    add_column :reservations, :reservation_time, :datetime
  end
end

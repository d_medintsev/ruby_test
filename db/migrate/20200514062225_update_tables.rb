class UpdateTables < ActiveRecord::Migration[6.0]
  def change
    remove_column :tables, :open, :string
    remove_column :tables, :close, :string
  end
end

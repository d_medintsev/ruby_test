class UpdateTableId < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :tables, column: :id
    add_column(:tables, :restaurant_id, :int)
    add_foreign_key :tables, :restaurants
  end
end

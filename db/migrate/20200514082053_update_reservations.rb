class UpdateReservations < ActiveRecord::Migration[6.0]
  def change
    remove_foreign_key :reservations, column: :id
    add_column(:reservations, :table_id, :int)
    add_column(:reservations, :user_id, :int)
    add_foreign_key :reservations, :tables
    add_foreign_key :reservations, :users
    remove_column(:reservations, :client)
  end
end

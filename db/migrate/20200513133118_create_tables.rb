class CreateTables < ActiveRecord::Migration[6.0]
  def change
    create_table :tables do |t|
      t.string :name
      t.string :open, :default => "10:00"
      t.string :close, :default => "23:00"
    end
    add_foreign_key :tables, :restaurants, column: :id
  end
end

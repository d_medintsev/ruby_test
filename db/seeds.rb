Restaurant.create(name: "Domino", address: "Kyiv 1")
Restaurant.create(name: "Penguin", address: "Kyiv 2")

Table.create(name: "Domino_1", restaurant_id: 33)
Table.create(name: "Domino_3", restaurant_id: 33)
Table.create(name: "Domino_4", restaurant_id: 33)
Table.create(name: "Domino_2", restaurant_id: 33)

Table.create(name: "Penguin_1", restaurant_id: 34)
Table.create(name: "Penguin_2", restaurant_id: 34)
Table.create(name: "Penguin_3", restaurant_id: 34)
Table.create(name: "Penguin_4", restaurant_id: 34)

User.create(name: "Jain", phone: "123456988")
User.create(name: "Rick", phone: "466852654")

Reservation.create(reservation_time: "15:00", user_id: 1, table_id: 1)
Reservation.create(reservation_time: "15:00", user_id: 1, table_id: 1)
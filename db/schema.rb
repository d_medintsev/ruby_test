# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# This file is the source Rails uses to define your schema when running `rails
# db:schema:load`. When creating a new database, `rails db:schema:load` tends to
# be faster and is potentially less error prone than running all of your
# migrations from scratch. Old migrations may fail to apply correctly if those
# migrations use external dependencies or application code.
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 2020_05_20_140222) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "reservations", force: :cascade do |t|
    t.integer "table_id"
    t.integer "user_id"
    t.datetime "reservation_time"
    t.index ["id"], name: "index_reservations_on_id"
  end

  create_table "restaurants", force: :cascade do |t|
    t.string "name"
    t.string "address"
    t.time "open", default: "2000-01-01 09:00:00"
    t.time "close", default: "2000-01-01 23:00:00"
  end

  create_table "tables", force: :cascade do |t|
    t.string "name"
    t.integer "restaurant_id"
  end

  create_table "users", id: :bigint, default: -> { "nextval('clients_id_seq'::regclass)" }, force: :cascade do |t|
    t.string "name"
    t.string "phone"
  end

  add_foreign_key "reservations", "tables"
  add_foreign_key "reservations", "users"
  add_foreign_key "tables", "restaurants"
end

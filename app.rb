require 'sinatra'
require "sinatra/namespace"
require 'sinatra/activerecord'
require './models'
require 'time'
require 'json'


get '/' do
  'Hello world!'
end

namespace '/api/v1' do

  before do
    content_type 'application/json'
  end

  # Show all restaurants in network
  get '/restaurants' do
    '[{"Action result": "OK", "Message": "Show all restaurants in network", "Data": %s}]' \
                  % [Restaurant.all.to_json]
  end

  # Add new restaurant
  post '/restaurants' do
    '[{"Action result": "OK", "Message": "Show all restaurants in network", "Data": %s}]' \
                   % [Restaurant.create(:name => @params['name'],
                                        :address => @params['address'],
                                        :open => @params['open'],
                                        :close => @params['close'])]
  end

  # Get Info about restaurant with id XX
  get '/restaurants/:id' do |id|
    '[{"Action result": "OK", "Message": "Get Info about restaurant with id %s", "Data": %s}]' \
                   % [id, Restaurant.all.find_by(:id => id).to_json]
  end

  # Edit Info about restaurant with id XX
  put '/restaurants/:id' do |id|
    obj = Restaurant.find_by(id: id)
    obj.update(:name => @params['name'],
               :address => @params['address'],
               :open => @params['open'],
               :close => @params['close'])
    '[{"Action result": "OK", "Message": "Edit Info about restaurant with id %s", "Data": ""}]' % [id]
  end

  # Delete restaurant Info from DB with id XX
  delete '/restaurants/:id' do |id|
    obj = Restaurant.find_by(id: id)
    obj.destroy
    '[{"Action result": "OK", "Message": "Delete restaurant Info from DB with id %s"}, "Data": ""]' % [id]
  end

  # Show all tables in restaurant with id XX
  get '/restaurants/:id/tables' do |id|
    '[{"Action result": "OK", "Message": "Show all tables in restaurant with id %s", "Data": %s}]' \
                  % [id, Table.where(:restaurant_id => id).to_json]
  end

  # Show reservation schedule for all tables in restaurant with id XX for selected date
  get '/restaurants/:id/tables/date/:date' do |id, date|
    restaurant = Restaurant.where(id: id).as_json
    opn = Time.parse(restaurant[0]["open"])
    cls = Time.parse(restaurant[0]["close"])
    total = (cls - opn) / 60 / 60 / 0.5
    reservations = {}
    tables = Table.all.select(:id, :name).where(:restaurant_id => id).as_json
    tables.each { |dat| reservations[dat["id"].to_s + "__" + dat["name"]] = [] }
    rrr = Reservation.find_by_sql("SELECT reservations.id as id, tables.id as table_id, tables.name as table_name, reservation_time,tables.restaurant_id, restaurants.name as restaurant_name FROM reservations
                                  join tables on tables.id = reservations.table_id
                                  join restaurants on restaurants.id =  tables.restaurant_id
                                  WHERE table_id in (select tables.id from tables where restaurant_id = " + id + ")").as_json
    rrr.each { |j|

      if Time.parse(j["reservation_time"]).strftime("%Y-%m-%d") == date
        reservations[j["table_id"].to_s + "__" + j["table_name"]] << Time.parse(j["reservation_time"]).strftime("%H:%M")
      end
    }
    result = {}
    reservations.each_key { |key|
      id = key.split("__")[0]
      name = key.split("__")[1]
      result[name] = []

      (0..total - 1).each { |i|
        tmp = {}
        dd = (opn + i * 30 * 60).strftime("%H:%M")
        tmp["time"] = dd
        tmp["table"] = name
        tmp["table_id"] = id.to_i
        tmp["status"] = !!(reservations[key].include?(dd)) ? "reserved" : "free"
        result[name] << tmp
      }
    }

    '[{"Action result": "OK", "Message": "Show reservation schedule for all tables in =%s= restaurant for selected date", "Data": %s}]' \
                  % [restaurant[0]["name"], result.to_json]
  end

  # Add new table to restaurant with id XX
  post '/restaurants/:id/tables' do |id|
    Table.create(:name => @params['name'],
                 :restaurant_id => id)
    '[{"Action result": "OK", "Message": "Add new table to restaurant with id %s", "Data": ""}]' \
                  % [id]
  end

  # Show all reservations for selected table in selected restaurant
  get '/restaurants/:id/tables/:table_id' do |id, table_id|
    result = Reservation.select(" reservations.id as id, users.name as client, reservations.reservation_time, tables.name as table, restaurants.name as restaurant ")
                 .joins("join tables on  tables.id= reservations.table_id")
                 .joins("join users on  users.id= reservations.user_id")
                 .joins("join restaurants on tables.restaurant_id = restaurants.id")
                 .where(table_id: table_id)
    '[{"Action result": "OK", "Message": "Show all reservations for selected table(id: %s)  in selected restaurant(id: %s", "Data": %s}]' \
                  % [table_id, id, result.to_json]
  end

  # Delete table with id YY in restaurant with id XX from DB
  delete '/restaurants/:id/tables/:table_id' do |id, table_id|
    obj = Table.find_by(id: table_id)
    obj.destroy
    '[{"Action result": "OK", "Message": "Delete table with id %s in restaurant with id %s from DB "}, "Data": ""]' \
                  % [table_id, id]
  end

  # Edit table Info with id YY in restaurant with id XX
  put '/restaurants/:id/tables/:table_id' do |id, table_id|
    obj = Table.find_by(id: table_id)
    obj.update(:name => @params['name'])
    '[{"Action result": "OK", "Message": "Edit table Info with id %s in restaurant with id %s", "Data": ""}]' \
                  % [table_id, id]
  end

  # Show reservation schedule for selected table(id: YY) in restaurant with id XX for selected date
  get '/restaurants/:id/tables/:table_id/date/:date' do |id, table_id, date|
    restaurant = Restaurant.where(id: id).as_json
    opn = Time.parse(restaurant[0]["open"])
    cls = Time.parse(restaurant[0]["close"])
    total = (cls - opn) / 60 / 60 / 0.5
    reservations = {}
    ii = 0
    Reservation.select(:reservation_time).where(table_id: table_id).as_json.each { |j|
      if Time.parse(j["reservation_time"]).strftime("%Y-%m-%d") == date
        reservations[ii] = Time.parse(j["reservation_time"]).strftime("%H:%M")
        ii += 1
      end
    }
    result = {}
    (0..total - 1).each { |i|
      tmp = {}
      dd = (opn + i * 30 * 60).strftime("%H:%M")
      ss = reservations.has_value?(dd)
      tmp["time"] = dd
      tmp["status"] = ss ? "reserved" : "free"
      result[i] = tmp
    }
    '[{"Action result": "OK", "Message": "Show reservation schedule for selected table(id: %s) in restaurant with id %s for selected date", "Data": %s}]' \
                  % [table_id, id, result.to_json]
  end

  # Show all clients registered in restaurant network
  get '/users' do
    User.all.to_json
    '[{"Action result": "OK", "Message": "Show all clients registered in restaurant network", "Data": %s}]' \
                  % [User.all.to_json]
  end

  # Add new client to the restaurant network
  post '/users' do
    User.create(:name => @params['name'],
                :phone => @params['phone'])
    '[{"Action result": "OK", "Message": "Add new client to the restaurant network", "Data": ""}]'
  end

  # Get user Info with userId XX
  get '/users/:user_id' do |user_id|
    '[{"Action result": "OK", "Message": "Get user Info with userId %s", "Data": ""}]' \
                  % [User.where(id: user_id).to_json]
  end

  # Update user Info with userId XX
  put '/users/:user_id' do |user_id|
    obj = User.find_by(id: user_id)
    obj.update(:name => @params['name'],
               :phone => @params['phone'])
    '[{"Action result": "OK", "Message": "Update user Info with userId %s", "Data": ""}]' % [user_id]
  end

  # Delete user from restaurant network with userId XX
  delete '/users/:user_id' do |user_id|
    obj = User.find_by(id: user_id)
    obj.destroy
    '[{"Action result": "OK", "Message": "Delete user from restaurant network with userId %s"}, "Data": ""]' % [user_id]
  end

  # Get all reservations in restaurant network
  get '/reservations' do
    result = Reservation.select(" users.name as client, reservations.id, reservations.reservation_time, tables.name as table, restaurants.name as restaurant ")
                 .joins("join tables on  tables.id= reservations.table_id")
                 .joins("join users on  users.id= reservations.user_id")
                 .joins("join restaurants on tables.restaurant_id = restaurants.id")
    result.to_json
    '[{"Action result": "OK", "Message": "SGet all reservations in restaurant network", "Data": ""}]'
  end

  # Get all reservations for user with id XX
  get '/reservations/users/:user_id' do |user_id|
    usr = User.where(id: user_id)[0].as_json
    result = Reservation.select(" reservations.id, reservations.reservation_time, tables.name as table, restaurants.name as restaurant ")
                 .joins("join tables on  tables.id= reservations.table_id")
                 .joins("join restaurants on tables.restaurant_id = restaurants.id").where(user_id: user_id)

    if result.length > 0
      dd = {}
      dd["user"] = usr
      dd["reservations"] = result[0].as_json
      '[{"Action result": "OK", "Message": "Get all reservations for user with id %s", "Data": %s}]' \
                  % [user_id, dd.to_json]
    else
      '[{"Action result": "OK", "Message": "Selected user with id %s have no reservation till now", "Data": ""}]' % [user_id]
    end

  end

  def has_reserved(reserved_list, user_id, selected_time, selected_rest_id, table_name)
    time_list = []
    table_list = []
    reserved_list.each { |reserv|
      if reserv["client"] == user_id
        time_list << Time.parse(reserv["reservation_time"]).strftime("%H:%M")
        table_list << reserv["table"]
      end
    }
    if !table_list.include?(table_name)
      return "free"
    end
    reserved_list.each { |res|
      time = Time.parse(res["reservation_time"]).strftime("%H:%M") == selected_time
      id = res["client"] == user_id
      place = res["rest_id"] == selected_rest_id
      selected_table = table_list.include?(res["table"]) && res["table"] == table_name
      if time && place && id && selected_table
        return "self reserved"
      elsif time && !place && !id
        return "reserved"
      elsif time && !place
        return "blocked"
      end
    }
    return "free"
  end

  # get reservations of user with id XX for selected date
  get '/reservations/users/:user_id/date/:date' do |user_id, date|
    restaurants = Restaurant.all.as_json
    table_list = {}
    Table.all.as_json.each { |tbl|
      if table_list[tbl["restaurant_id"]].nil?
        table_list[tbl["restaurant_id"]] = []
        table_list[tbl["restaurant_id"]] << tbl["id"].to_s + "__" + tbl["name"]
      else
        table_list[tbl["restaurant_id"]] << tbl["id"].to_s + "__" + tbl["name"]
      end
    }
    reservations = Reservation.find_by_sql("SELECT  users.id as client, reservations.reservation_time,
    tables.name as table, restaurants.id as rest_id FROM reservations
    JOIN tables on  tables.id= reservations.table_id
    JOIN users on  users.id= reservations.user_id
    JOIN restaurants on tables.restaurant_id = restaurants.id WHERE DATE(reservation_time) = '" + date + "'").as_json
    result = {}
    restaurants.each { |rest|
      result[rest["name"]] = {}
      opn = Time.parse(rest["open"])
      cls = Time.parse(rest["close"])
      total = (cls - opn) / 60 / 60 / 0.5
      if !table_list.has_key?(rest["id"])
        next
      end
      table_list[rest["id"]].each { |table_name|
        id = table_name.split("__")[0]
        name = table_name.split("__")[1]
        result[rest["name"]][name] = []
        (0..total - 1).each { |i|
          tmp = {}
          dd = (opn + i * 30 * 60).strftime("%H:%M")
          tmp["time"] = dd
          tmp["table"] = name
          tmp["table_id"] = id.to_i
          tmp["status"] = has_reserved(reservations, user_id.to_i, dd, rest["id"], name)
          # uncomment this block if you don't want to show tables with status free
          # if tmp["status"] == "free"
          #   next
          # end
          result[rest["name"]][name] << tmp
        }
      }
    }
    result.to_json

  end

  # Add new reservations
  post '/reservations' do
    dd = Reservation.all.where(:table_id => @params["table_id"], :reservation_time => @params["reservation_time"]).as_json
    if dd.count == 0
      Reservation.create(:table_id => @params["table_id"],
                         :user_id => @params["user_id"],
                         :reservation_time => @params["reservation_time"])
      '[{"Action result": "OK", "Message": "Create new reservations is DONE"}, "Data": ""]'
    else
      '[{"Action result": "ERROR", "Message": "Create new reservations is FAIL. Selected time is reserved", "Data": ""}]'
    end
  end

  # Get full Info about reservation with id XX
  get '/reservations/:res_id' do |res_id|
    result = Reservation.select(" users.name as client, users.phone as contacts, reservations.id, reservations.reservation_time, tables.name as table, restaurants.name as restaurant ")
                 .joins("join tables on  tables.id= reservations.table_id")
                 .joins("join users on  users.id= reservations.user_id")
                 .joins("join restaurants on tables.restaurant_id = restaurants.id").where(id: res_id)

    '[{"Action result": "OK", "Message": "Get full Info about reservation with id %s", "Data": %s}]' \
                  % [res_id, result.to_json]
  end

  # Delete reservation with id XX
  delete 'reservations/:res_id' do |res_id|
    obj = Reservation.find_by(id: res_id)
    obj.destroy
    '[{"Action result": "OK", "Message": "Delete reservation with id %s", "Data": ""}]'
  end

  # Select all reservations at selected date
  get '/reservations/date/:date' do
    result = Reservation.find_by_sql("SELECT tables.id as table_id, tables.name, reservation_time,tables.restaurant_id, restaurants.name FROM reservations
    join tables on tables.id = reservations.table_id
    join restaurants on restaurants.id =  tables.restaurant_id")
    '[{"Action result": "OK", "Message": "Select all reservations at selected date", "Data": %s}]' % [result.to_json]
  end
end
